package com.example.debate.Entidades;

import java.io.Serializable;

public class Persona implements Serializable {




    //Serializable permite enviar un objeto
    //private Integer id;
    private String Nombre;
    private String Titulo;
   private String Argumento;

    public Persona(String nombre,String titulo,String argumento) {
        this.Nombre = nombre;
        this.Titulo=titulo;
        this.Argumento=argumento;
    }

    public Persona (){

    }

    public String getNombre() {

        return Nombre;
    }

    public void setNombre(String nombre) {

        Nombre = nombre;
    }

    public String getTitulo() {

        return Titulo;
    }
    public void setTitulo(String titulo) {

        Titulo = titulo;
    }


    public String getArgumento() {
        return Argumento;
    }

    public void setArgumento(String argumento) {
        Argumento = argumento;
    }

}
