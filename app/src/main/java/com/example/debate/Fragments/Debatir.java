package com.example.debate.Fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.debate.Entidades.Persona;
import com.example.debate.MainActivity;
import com.example.debate.R;

import org.w3c.dom.Text;


public class Debatir extends Fragment {

    Button boton3,boton4,boton5;
    TextView vista4,vista5,vista6,vista7,vista8;
    EditText texto1,texto3;
    Spinner spinner;
    String opcion;
    LinearLayout linearLayout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.debatir, container, false);
        texto1=(EditText)view.findViewById(R.id.ediText);
        texto3=(EditText)view.findViewById(R.id.editText3);
        boton5=boton4=(Button)view.findViewById(R.id.button5);
        boton3=boton4=(Button)view.findViewById(R.id.button3);
        boton4=(Button)view.findViewById(R.id.button4);
        vista4=(TextView)view.findViewById(R.id.textView4);
        vista5=(TextView)view.findViewById(R.id.textView5);
        vista6=(TextView)view.findViewById(R.id.textView6);
        vista7=(TextView)view.findViewById(R.id.textView7);
        vista8=(TextView)view.findViewById(R.id.textView8);
        spinner=(Spinner)view.findViewById(R.id.spinner);
        linearLayout=(LinearLayout)view.findViewById(R.id.linear);
        vista5.setVisibility(View.GONE);
        vista7.setVisibility(View.GONE);
        vista8.setVisibility(view.GONE);
        ArrayAdapter<CharSequence>adapter=ArrayAdapter.createFromResource (getActivity(), R.array.opciones,android.R.layout.simple_spinner_item);
        spinner.setAdapter(adapter);
        boton5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vista5.setVisibility(View.VISIBLE);
                vista7.setVisibility(View.VISIBLE);
                vista5.setText("1");
                vista7.setText("0");
            }
        });
        boton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                vista5.setVisibility(View.VISIBLE);
                vista7.setVisibility(View.VISIBLE);
                vista5.setText("0");
                vista7.setText("1");
            }
        });
        boton4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // texto1.setTypeface(null, Typeface.BOLD);
                String usuario=texto1.getText().toString();
                String comentario=texto3.getText().toString();
                if (usuario.isEmpty()||comentario.isEmpty()){
                    Toast.makeText(getActivity(),"faltan campos por completar",Toast.LENGTH_SHORT).show();

                }

               //*creo textview al hacer click en el boton
                TextView vista8=new TextView(getActivity());//*
                vista8.setTypeface(null, Typeface.NORMAL);
                vista8.setTextColor(Color.BLACK);
                vista8.setText(usuario+":"+comentario);
                vista8.setVisibility(View.VISIBLE);
                vista8.setBackgroundResource(R.drawable.borde);//cada vez que apreto el boton se genera un comentario con diseño
                linearLayout.addView(vista8);//*
                texto1.setVisibility(View.INVISIBLE);
                spinner.setVisibility(View.INVISIBLE);
                texto3.setText("");

            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                opcion=spinner.getSelectedItem().toString();
                if(opcion.equals("Anónimamente")){
                    texto1.setHint("Ingrese nombre anónimo");
                    texto1.setText("");
                    texto3.setText("");
                    texto1.setEnabled(true);
                    texto3.setEnabled(true);

                }
                else if (opcion.equals(("Nombre de usuario"))){
                    texto1.setHint("Ingrese nombre de usuario real");
                    texto1.setText("");
                    texto3.setText("");
                    texto1.setEnabled(true);
                    texto3.setEnabled(true);

                }
                else{
                    texto1.setEnabled(false);
                    texto3.setEnabled(false);


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*
        //recibiendo objeto enviado por Bundle
        Bundle objeto=getArguments();
        Persona persona=null;
        //validando si existen argumentos enviados
        if (objeto!=null ){
            persona=(Persona)objeto.getSerializable("objeto");
            //estableciendo datos

        }*/





        return view;
    }
}