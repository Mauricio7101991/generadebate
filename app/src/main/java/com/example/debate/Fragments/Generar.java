package com.example.debate.Fragments;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.example.debate.BDSQLITE.ConexionSQLiteHelper;
import com.example.debate.R;



public class Generar extends Fragment {

    RecyclerView recyclerViewPersonas;
    Button boton1,boton2;
    Spinner spinner;
    EditText texto1,texto2,texto3,texto4,texto5;
    TextView textView;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       View view=inflater.inflate(R.layout.fragment_generar, container, false);
        boton2=(Button)view.findViewById(R.id.button2);
        texto1=(EditText)view.findViewById(R.id.editTextTextPersonName);
        texto3=(EditText)view.findViewById(R.id.editTextTextPersonName3);
        texto4=(EditText)view.findViewById(R.id.editTextTextMultiLine);


        boton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre=texto1.getText().toString();
                String titulo=texto3.getText().toString();
                String argu=texto4.getText().toString();
                ConexionSQLiteHelper conn=new ConexionSQLiteHelper(getContext(),"usuario",null,1);
                SQLiteDatabase db=conn.getWritableDatabase();
                if (!titulo.isEmpty()&&!titulo.isEmpty()&&!argu.isEmpty() ){
                    ContentValues valor=new ContentValues();
                    valor.put("usuario",nombre);
                    valor.put("titulo",titulo);
                    valor.put("argumento",argu);
                    long id=db.insert("usuarios",null,valor);
                    Toast.makeText(getActivity(),"el dato esta guardado en la id "+id, Toast.LENGTH_SHORT).show();
                    db.close();
                }
                else{
                    Toast.makeText(getActivity(),"faltan completar datos",Toast.LENGTH_SHORT).show();
                }
                texto1.setText("");
                texto3.setText("");
                texto4.setText("");
                db.close();


                }



        });



        return view;
    }

}