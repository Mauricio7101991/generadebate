package com.example.debate.Fragments;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.debate.Adaptadores.AdapterPersona;
import com.example.debate.BDSQLITE.ConexionSQLiteHelper;
import com.example.debate.ComunicaFragment;
import com.example.debate.Entidades.Persona;
import com.example.debate.R;


import java.util.ArrayList;



public class Listar extends Fragment {
    AdapterPersona adapterPersona;
    RecyclerView recyclerViewPersonas;//referencia a recyclerview
    ArrayList<Persona>listapersonas;
    ConexionSQLiteHelper conn;//instancia a la bd
    //referencias para comunicar fragments
    Activity activividad;
    ComunicaFragment comunicaFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_listar, container, false);

        conn=new ConexionSQLiteHelper(getActivity().getApplicationContext(),"usuario",null,1);
        listapersonas=new ArrayList<>();
        recyclerViewPersonas=(RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerViewPersonas.setLayoutManager(new LinearLayoutManager(getContext()));
        consultarListaPersonas();


        adapterPersona=new AdapterPersona(getActivity().getApplicationContext(),listapersonas);
        recyclerViewPersonas.setAdapter(adapterPersona);
        adapterPersona.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comunicaFragment.enviarPersona(listapersonas.get(recyclerViewPersonas.getChildAdapterPosition(v)));
            }
        });


        return view;
    }
   private void consultarListaPersonas() {
        SQLiteDatabase db=conn.getReadableDatabase();
        Persona persona=null;
        Cursor cursor=db.rawQuery("select * from usuarios",null);
        while (cursor.moveToNext()){
            persona=new Persona();
            persona.setNombre(cursor.getString(1));
            persona.setTitulo(cursor.getString(2));
            persona.setArgumento(cursor.getString(3));


            listapersonas.add(persona);

    }
        db.close();
    }

    @Override
    public void onAttach(@NonNull Context context) {//establece comunicación entre las lista/fragment y el fragement Debatir
        super.onAttach(context);
        if(context instanceof Activity){// si el contexto que llega es una instancia de un activity
            this.activividad=(Activity)context;
            comunicaFragment=(ComunicaFragment)this.activividad;

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}








